package tman

import (
	"log"
	"net/http/httptest"
	"os"
	"testing"
)

const (
	testBody = `<!DOCTYPE html>
<html lang="en">
    <head><title>TMan Test</title></head>
    <body>
        <h1>TMan Test</h1>
    </body>
</html>`
)

func TestTMan(t *testing.T) {
	tm := NewVault(nil, false, nil, os.DirFS("tpls"))
	tm.RegisterPage(&Page{Key: "test", Tpls: []string{"test.gohtml"}})

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/test", nil)

	tm.RenderPage(w, r, "test", nil)

	if w.Code != 200 {
		t.Errorf("Got %d expected 200", w.Code)
	}

	if w.Body == nil {
		t.Error("expected a body")
	}

	bodyValue := string(w.Body.Bytes())
	log.Println(bodyValue)
	if bodyValue != testBody {
		t.Errorf("body not correct got %s", bodyValue)
	}

	if testPage, ok := tm.pages["test"]; ok {
		if testPage.Cached != nil {
			t.Error("expected page's template to be nil")
		}
	}
}

func TestTManProd(t *testing.T) {
	tm := NewVault(nil, true, nil, os.DirFS("tpls"))
	tm.RegisterPage(&Page{Key: "test", Tpls: []string{"test.gohtml"}})

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/test", nil)

	tm.RenderPage(w, r, "test", nil)

	if w.Code != 200 {
		t.Errorf("Got %d expected 200", w.Code)
	}

	if w.Body == nil {
		t.Error("expected a body")
	}

	bodyValue := string(w.Body.Bytes())
	log.Println(bodyValue)
	if bodyValue != testBody {
		t.Errorf("body not correct got %s", bodyValue)
	}

	if testPage, ok := tm.pages["test"]; ok {
		if testPage.Cached == nil {
			t.Error("expected page's template to not be nil")
		}
	}
}
