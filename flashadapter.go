package tman

import "net/http"

type FlashAdapter interface {
	SetErrorFlash(w http.ResponseWriter, r *http.Request, msg string) error
	SetSuccessFlash(w http.ResponseWriter, r *http.Request, msg string) error
	GetFlashMessages(w http.ResponseWriter, r *http.Request) (e string, s string, err error)
}
