module gitlab.com/high-creek-software/tman

go 1.19

require (
	github.com/microcosm-cc/bluemonday v1.0.4
	github.com/yuin/goldmark v1.3.2
)
