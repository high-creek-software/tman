package tman

import (
	"bytes"
	"html/template"
	"log"

	"github.com/microcosm-cc/bluemonday"
	"github.com/yuin/goldmark"
)

func SafeHTML(input string) template.HTML {
	return template.HTML(input)
}

func MRender(in string) template.HTML {
	var buf bytes.Buffer
	err := goldmark.Convert([]byte(in), &buf)
	if err != nil {
		log.Println("error converting markdown", err)
		return template.HTML("")
	}
	return template.HTML(bluemonday.UGCPolicy().SanitizeBytes(buf.Bytes()))
}
