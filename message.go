package tman

import (
	"html/template"
	txttpl "text/template"
)

type Message struct {
	Key         string   `json:"key"`
	PlainTPLS   []string `json:"plainTpls"`
	CachedPlain *txttpl.Template
	HtmlTPLS    []string `json:"htmlTpls"`
	CachedHtml  *template.Template
}

func NewPlainMessage(key string, plainTpls ...string) *Message {
	return &Message{Key: key, PlainTPLS: plainTpls}
}

func NewHtmlMessage(key string, htmlTpls ...string) *Message {
	return &Message{Key: key, HtmlTPLS: htmlTpls}
}
