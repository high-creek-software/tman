package tman

type Asset struct {
	Key      string `json:"key"`
	DevPath  string `json:"devPath"`
	ProdPath string `json:"prodPath"`
}

func NewAsset(key, devPath, prodPath string) Asset {
	return Asset{Key: key, DevPath: devPath, ProdPath: prodPath}
}
