package tman

import "html/template"

type Page struct {
	Key      string   `json:"key"`
	fileName string   `json:"mainFileName"`
	Tpls     []string `json:"tpls"`
	Cached   *template.Template

	Title              string `json:"title"`
	Description        string `json:"description"`
	Canonical          string `json:"canonical"`
	OgURL              string `json:"ogurl"`
	OgTitle            string `json:"ogtitle"`
	OgDescription      string `json:"ogdescription"`
	OgImage            string `json:"ogimage"`
	OgImageType        string `json:"ogimagetype"`
	OgImageWidth       string `json:"ogimagewidth"`
	OgImageHeight      string `json:"ogimageheight"`
	FbAppID            string `json:"fbappid"`
	OgType             string `json:"ogtype"`
	OgLocale           string `json:"oglocale"`
	TwitterCard        string `json:"twittercard"`
	TwitterSite        string `json:"twittersite"`
	TwitterTitle       string `json:"twittertitle"`
	TwitterDescription string `json:"twitterdescription"`
	TwitterCreator     string `json:"twittercreator"`
	TwitterImage       string `json:"twitterimage"`
}

type PageOpts func(page *Page)

func NewPage(key string, tpls ...string) *Page {
	return &Page{Key: key, fileName: tpls[0], Tpls: tpls}
}

func (p *Page) SetOptions(opts ...PageOpts) *Page {
	for _, po := range opts {
		po(p)
	}
	return p
}

func SetTitle(title string) PageOpts {
	return func(p *Page) {
		p.Title = title
	}
}

func SetDescription(description string) PageOpts {
	return func(p *Page) {
		p.Description = description
	}
}

// GetTitle returns the page title, intended to be called from the template with a default value
func (p Page) GetTitle(def string) string {
	if p.Title == "" {
		return def
	}
	return p.Title
}

// GetDescription returns page description, intended to be called from the template with a default value
func (p Page) GetDescription(def string) string {
	if p.Description == "" {
		return def
	}
	return p.Description
}

// GetOgUrl returns the open graph url or the canonical url
func (p Page) GetOgUrl() string {
	if p.OgURL != "" {
		return p.OgURL
	}
	return p.Canonical
}

// GetOgTitle returns the open graph title for the page, or the regular title, or the default value passed in
func (p Page) GetOgTitle(def string) string {
	if p.OgTitle != "" {
		return p.OgTitle
	} else if p.Title != "" {
		return p.Title
	} else {
		return def
	}
}

// GetOgDescription returns the open graph description, the regular description, or the default value passed in
func (p Page) GetOgDescription(def string) string {
	if p.OgDescription != "" {
		return p.OgDescription
	} else if p.Description != "" {
		return p.Description
	} else {
		return def
	}
}

// GetOgImage returns the open graph image or the default value passed in
func (p Page) GetOgImage(def string) string {
	if p.OgImage != "" {
		return p.OgImage
	}
	return def
}

// GetFbAppId returns the fb app id or the default value passed in
func (p Page) GetFbAppId(def string) string {
	if p.FbAppID != "" {
		return p.FbAppID
	}
	return def
}

// GetOgType returns the open graph type or the default website
func (p Page) GetOgType() string {
	if p.OgType != "" {
		return p.OgType
	}
	return "website"
}

// GetOgLocale returns the open graph locale or the default locale
func (p Page) GetOgLocale() string {
	if p.OgLocale != "" {
		return p.OgLocale
	}
	return "en_US"
}

// GetTwitterCard returns the twitter card summary
func (p Page) GetTwitterCard(def string) string {
	if p.TwitterCard != "" {
		return p.TwitterCard
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterSite returns the twitter site or default.  i.e. @KendellFab
func (p Page) GetTwitterSite(def string) string {
	if p.TwitterSite != "" {
		return p.TwitterSite
	}
	return def
}

// GetTwitterTitle returns the twitter title or the page title or the default
func (p Page) GetTwitterTitle(def string) string {
	if p.TwitterTitle != "" {
		return p.TwitterTitle
	} else if p.Title != "" {
		return p.Title
	}
	return def
}

// GetTwitterDescription returns the twitter description or the page description or the default
func (p Page) GetTwitterDescription(def string) string {
	if p.TwitterDescription != "" {
		return p.TwitterDescription
	} else if p.Description != "" {
		return p.Description
	}
	return def
}

// GetTwitterCreator returns the twitter creator or default
func (p Page) GetTwitterCreator(def string) string {
	if p.TwitterCreator != "" {
		return p.TwitterCreator
	}
	return def
}

// GetTwitterImage returns the twitter image or default
func (p Page) GetTwitterImage(def string) string {
	if p.TwitterImage != "" {
		return p.TwitterImage
	}
	return def
}
