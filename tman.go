package tman

import (
	"bytes"
	"encoding/json"
	"errors"
	"html/template"
	"io"
	"io/fs"
	"log"
	"net/http"
	"path"
	"path/filepath"
	"strings"
	"sync"
	txttpl "text/template"
)

const (
	DefaultPageGlob    = "*.page.*"
	DefaultLayoutGlob  = "*.layout.*"
	DefaultPartialGlob = "*.partial.*"
)

var ErrorNoTemplate = errors.New("error no template")
var ErrorPartialNotFound = errors.New("partial not found")
var ErrorParsingPartial = errors.New("error parsing partial")
var ErrorMessageNotFound = errors.New("error message not found")
var ErrorNoTemplates = errors.New("error no templates")
var ErrorParsingMessage = errors.New("error parsing message")

type Merge func(r *http.Request, data map[string]interface{})

// Vault is a wrapper around the go template library.
// The pageFS parameter is to be the root file system used where all of the page & partial tpls reference from
type Vault struct {
	flashAdapter FlashAdapter
	prod         bool
	pageFs       fs.FS
	pages        map[string]*Page
	partials     map[string]*Partial
	tplFuncs     map[string]interface{}
	config       interface{}
	merge        Merge
	messageFs    fs.FS
	messages     map[string]*Message

	assetJs  map[string]Asset
	assetCss map[string]Asset
	assetImg map[string]Asset

	pageMutex    *sync.RWMutex
	partialMutex *sync.RWMutex
	messageMutex *sync.RWMutex

	pageGlob    string
	layoutGlob  string
	partialGlob string
}

type VaultConfig func(*Vault)

// NewVault creates a new Vault instance.
func NewVault(flashAdapter FlashAdapter, prod bool, config interface{}, pageFs fs.FS, opts ...VaultConfig) *Vault {
	v := &Vault{flashAdapter: flashAdapter,
		prod:         prod,
		pageFs:       pageFs,
		pages:        make(map[string]*Page),
		partials:     make(map[string]*Partial),
		tplFuncs:     make(map[string]interface{}),
		config:       config,
		pageMutex:    &sync.RWMutex{},
		partialMutex: &sync.RWMutex{},
		messages:     make(map[string]*Message),
		messageMutex: &sync.RWMutex{},
		assetJs:      make(map[string]Asset),
		assetCss:     make(map[string]Asset),
		assetImg:     make(map[string]Asset),
		pageGlob:     DefaultPageGlob,
		layoutGlob:   DefaultLayoutGlob,
		partialGlob:  DefaultPartialGlob,
	}

	for _, opt := range opts {
		opt(v)
	}

	v.tplFuncs["partial"] = v.Partial
	v.tplFuncs["safeHtml"] = SafeHTML
	v.tplFuncs["md"] = MRender
	v.tplFuncs["js"] = v.GetJsPath
	v.tplFuncs["css"] = v.GetCssPath
	v.tplFuncs["img"] = v.GetImgPath

	return v
}

// AutoRegisterPages will look for pages, layouts, and partials based upon the glob patterns that are set.
// You need your templates setup properly for this to work, but it does make it easier to get started.
// base.layout.gohtml
// {{define "base"}}
// <!DOCTYPE html>
//
//	<html lang="en">
//		<head>
//			<title>{{block "title" .}}Title{{end}}</title>
//		</head>
//		<body>
//			{{block "content" .}}{{end}}
//		</body>
//	</html>
//
// {{end}} # Ends defining base template
//
// index.page.gohtml
// {{template "base" .}}
//
// {{define "title"}}Page Title{{end}}
// {{define "content"}}
//
//		<h1>Page</h1>
//	{{end}}
func (v *Vault) AutoRegisterPages() {
	var pages []string
	var aux []string

	fs.WalkDir(v.pageFs, ".", func(p string, dir fs.DirEntry, e error) error {
		if e != nil {
			log.Println("error for path", p)
			return e
		}

		if dir.IsDir() {
			return nil
		}

		pageMatch, pageErr := path.Match(v.pageGlob, dir.Name())
		if pageErr != nil {
			log.Println("error matching path", dir.Name())
		}
		if pageMatch {
			pages = append(pages, p)
		}

		layoutMatch, layoutErr := path.Match(v.layoutGlob, dir.Name())
		if layoutErr != nil {
			log.Println("error matching layout", dir.Name())
		}
		if layoutMatch {
			aux = append(aux, p)
		}

		partialMatch, partialErr := path.Match(v.partialGlob, dir.Name())
		if partialErr != nil {
			log.Println("error matching partial", dir.Name())
		}
		if partialMatch {
			aux = append(aux, p)
		}

		return nil
	})

	for _, page := range pages {

		baseName := filepath.Base(page)
		parts := strings.Split(baseName, ".")
		name := parts[0]

		// For some reason the main page name needs to be added last, for the default block content to be overrided.
		tpls := aux
		tpls = append(tpls, page)
		pg := NewPage(name, tpls...)
		pg.fileName = page
		v.RegisterPage(pg)
	}
}

func (v *Vault) RegisterPage(page *Page) {
	v.pages[page.Key] = page
}

func (v *Vault) RegisterPartial(partial *Partial) {
	v.partials[partial.Key] = partial
}

func (v *Vault) RegisterMerge(merge Merge) {
	v.merge = merge
}

func (v *Vault) RegisterTemplateFunc(key string, fn any) {
	v.tplFuncs[key] = fn
}

// RegisterMessageFS takes a file system where the message tpls reference from
func (v *Vault) RegisterMessageFS(message fs.FS) {
	v.messageFs = message
}

func (v *Vault) RegisterMessage(message *Message) {
	v.messages[message.Key] = message
}

func (v *Vault) RegisterJS(js Asset) {
	v.assetJs[js.Key] = js
}

func (v *Vault) RegisterCSS(css Asset) {
	v.assetCss[css.Key] = css
}

func (v *Vault) RegisterImg(img Asset) {
	v.assetImg[img.Key] = img
}

func (v *Vault) GetJsPath(key string) string {
	asset, ok := v.assetJs[key]
	if !ok {
		return ""
	}

	if v.prod && asset.ProdPath != "" {
		return asset.ProdPath
	}
	return asset.DevPath
}

func (v *Vault) GetCssPath(key string) string {
	asset, ok := v.assetCss[key]
	if !ok {
		return ""
	}

	if v.prod && asset.ProdPath != "" {
		return asset.ProdPath
	}
	return asset.DevPath
}

func (v *Vault) GetImgPath(key string) string {
	asset, ok := v.assetImg[key]
	if !ok {
		return ""
	}

	if v.prod && asset.ProdPath != "" {
		return asset.ProdPath
	}
	return asset.DevPath
}

// AddJsonManifest is a json representation of the pages, partials, messages, and assets to be managed by this Vault instance
// It is to be formatted as
//
//	{
//			"pages": [],
//			"partials": [],
//			"messages": [],
//			"js": [],
//			"css": [],
//			"img": []
//		}
//
// It is recommended that this use the embed directive to get a string from the filesystem.  However it could be stored as a string and passed in.
func (v *Vault) AddJsonManifest(input string) error {

	type manifest struct {
		Pages    []*Page    `json:"pages"`
		Partials []*Partial `json:"partials"`
		Messages []*Message `json:"messages"`
		Js       []Asset    `json:"js"`
		Css      []Asset    `json:"css"`
		Img      []Asset    `json:"img"`
	}

	var m manifest

	err := json.Unmarshal([]byte(input), &m)
	if err != nil {
		return err
	}

	for _, page := range m.Pages {
		v.pages[page.Key] = page
	}

	for _, partial := range m.Partials {
		v.partials[partial.Key] = partial
	}

	for _, message := range m.Messages {
		v.messages[message.Key] = message
	}

	for _, js := range m.Js {
		v.assetJs[js.Key] = js
	}

	for _, css := range m.Css {
		v.assetCss[css.Key] = css
	}

	for _, img := range m.Img {
		v.assetImg[img.Key] = img
	}

	return nil
}

// Redirect returns a redirect with http.StatusSeeOther
func (v *Vault) Redirect(w http.ResponseWriter, r *http.Request, url string) {
	v.RedirectCode(w, r, url, http.StatusSeeOther)
}

// RedirectCode returns a redirect with given http status
func (v *Vault) RedirectCode(w http.ResponseWriter, r *http.Request, url string, code int) {
	http.Redirect(w, r, url, code)
}

// RenderJson is a shortcut method for rendering json to the response
func (v *Vault) RenderJson(w http.ResponseWriter, data any) {
	v.RenderJsonWithCode(http.StatusOK, w, data)
}

func (v *Vault) RenderJsonWithCode(code int, w http.ResponseWriter, data any) {
	if data, err := json.Marshal(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error rendering json"))
	} else {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(code)
		w.Write(data)
	}
}

func (v *Vault) acquireTemplate(key string) (*Page, *template.Template, error) {
	v.pageMutex.RLock()
	page, ok := v.pages[key]
	v.pageMutex.RUnlock()
	if !ok {
		return nil, nil, ErrorNoTemplate
	}
	if v.prod && page.Cached != nil {
		return page, page.Cached, nil
	}

	tpl, err := v.parseTemplate(page)
	if err != nil {
		return nil, nil, err
	}
	v.pageMutex.Lock()
	defer v.pageMutex.Unlock()
	if v.prod {
		page.Cached = tpl
	}
	return page, tpl, nil
}

func (v *Vault) parseTemplate(page *Page) (*template.Template, error) {
	return template.New(filepath.Base(page.fileName)).Funcs(v.tplFuncs).ParseFS(v.pageFs, page.Tpls...)
}

func (v *Vault) RenderPage(w http.ResponseWriter, r *http.Request, key string, data map[string]interface{}) {
	page, tpl, err := v.acquireTemplate(key)
	if err != nil {
		log.Println("error loading template", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(ErrorNoTemplate.Error()))
		return
	}

	if data == nil {
		data = make(map[string]interface{})
	}
	data["Config"] = v.config
	data["Request"] = r
	data["Page"] = page
	if v.flashAdapter != nil {
		if e, s, err := v.flashAdapter.GetFlashMessages(w, r); err == nil {
			if e != "" {
				data["FlashError"] = e
			}
			if s != "" {
				data["FlashSuccess"] = s
			}
		} else {
			log.Println("error loading flash messages", err)
			data["FlashLoadError"] = err.Error()
		}

	}

	if v.merge != nil {
		v.merge(r, data)
	}

	var doc bytes.Buffer
	if err = tpl.Execute(&doc, data); err == nil {
		w.WriteHeader(http.StatusOK)
		w.Write(doc.Bytes())
	} else {
		log.Println("error executing templates", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error rendering templates"))
	}

}

func (v *Vault) Render(w http.ResponseWriter, r *http.Request, key string, data any) {

	page, tpl, err := v.acquireTemplate(key)
	if err != nil {
		log.Println("error loading template", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(ErrorNoTemplate.Error()))
		return
	}

	payload := Payload{any: data, Config: v.config, Request: r, Page: page}

	if v.flashAdapter != nil {
		if e, s, err := v.flashAdapter.GetFlashMessages(w, r); err == nil {
			if e != "" {
				payload.FlashError = e
			}
			if s != "" {
				payload.FlashSuccess = s
			}
		} else {
			log.Println("error loading flash messages", err)
		}
	}

	if v.merge != nil {
		payload.Merge = make(map[string]any)
		v.merge(r, payload.Merge)
	}

	var doc bytes.Buffer
	if err = tpl.Execute(&doc, payload); err == nil {
		w.WriteHeader(http.StatusOK)
		w.Write(doc.Bytes())
	} else {
		log.Println("error executing templates", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("error rendering templates"))
	}
}

type Payload struct {
	any
	Config         any
	Request        *http.Request
	Page           *Page
	FlashError     string
	FlashSuccess   string
	FlashLoadError string
	Merge          map[string]any
}

func (v *Vault) RenderMessagePlain(w io.Writer, key string, data map[string]interface{}) error {
	v.messageMutex.RLock()
	message, ok := v.messages[key]
	v.messageMutex.RUnlock()
	if !ok {
		return ErrorMessageNotFound
	}

	if message.PlainTPLS == nil || len(message.PlainTPLS) == 0 {
		return ErrorNoTemplates
	}

	var tpl *txttpl.Template
	var err error

	if v.prod && message.CachedPlain != nil {
		tpl = message.CachedPlain
	} else {
		tpl, err = txttpl.New(filepath.Base(message.PlainTPLS[0])).Funcs(v.tplFuncs).ParseFS(v.messageFs, message.PlainTPLS...)
		if err != nil {
			log.Println("error parsing plain message", message.PlainTPLS, err)
			return ErrorParsingMessage
		}

		if v.prod {
			v.messageMutex.Lock()
			message.CachedPlain = tpl
			v.messageMutex.Unlock()
		}
	}

	return tpl.Execute(w, data)
}

func (v *Vault) RenderMessageHtml(w io.Writer, key string, data map[string]interface{}) error {
	v.messageMutex.RLock()
	message, ok := v.messages[key]
	v.messageMutex.RUnlock()
	if !ok {
		return ErrorMessageNotFound
	}

	if message.HtmlTPLS == nil || len(message.HtmlTPLS) == 0 {
		return ErrorNoTemplates
	}

	var tpl *template.Template
	var err error

	if v.prod && message.CachedHtml != nil {
		tpl = message.CachedHtml
	} else {
		tpl, err = template.New(filepath.Base(message.HtmlTPLS[0])).Funcs(v.tplFuncs).ParseFS(v.messageFs, message.HtmlTPLS...)
		if err != nil {
			log.Println("error parsing html message", message.HtmlTPLS)
			return ErrorParsingMessage
		}

		if v.prod {
			v.messageMutex.Lock()
			message.CachedHtml = tpl
			v.messageMutex.Unlock()
		}
	}

	return tpl.Execute(w, data)
}

// Partial allows a partial to be loaded within the template.
// Use: {{ partial "key" . }}
func (v *Vault) Partial(name string, payload interface{}) (template.HTML, error) {

	v.partialMutex.RLock()
	partial, ok := v.partials[name]
	v.partialMutex.RUnlock()
	if !ok {
		return "", ErrorPartialNotFound
	}

	var tpl *template.Template
	var err error

	if v.prod && partial.Cached != nil {
		tpl = partial.Cached
	} else {
		tpl, err = template.New(filepath.Base(partial.Tpl)).Funcs(v.tplFuncs).ParseFS(v.pageFs, partial.Tpl)
		if err != nil {
			log.Println("error parsing partial", partial.Tpl)
			return "", ErrorParsingPartial
		}

		if v.prod {
			v.partialMutex.Lock()
			partial.Cached = tpl
			v.partialMutex.Unlock()
		}
	}

	var buff bytes.Buffer
	execErr := tpl.Execute(&buff, payload)

	if execErr != nil {
		return "", execErr
	}

	return template.HTML(buff.Bytes()), nil
}

// SetErrorFlash sets a flash error message if the fazer.store is not nil
func (v *Vault) SetErrorFlash(w http.ResponseWriter, r *http.Request, message string) {
	if v.flashAdapter != nil {
		v.flashAdapter.SetErrorFlash(w, r, message)
	}
}

// SetSuccessFlash sets a flash success message if the fazer.store is not nil
func (v *Vault) SetSuccessFlash(w http.ResponseWriter, r *http.Request, message string) {
	if v.flashAdapter != nil {
		v.flashAdapter.SetSuccessFlash(w, r, message)
	}
}

func SetPageGlob(pageGlob string) func(v *Vault) {
	return func(v *Vault) {
		v.pageGlob = pageGlob
	}
}

func SetLayoutGlob(layoutGlob string) func(v *Vault) {
	return func(v *Vault) {
		v.layoutGlob = layoutGlob
	}
}

func SetPartialGlob(partialGlob string) func(v *Vault) {
	return func(v *Vault) {
		v.partialGlob = partialGlob
	}
}
