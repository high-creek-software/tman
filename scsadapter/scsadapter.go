package scsadapter

import (
	"net/http"

	"github.com/alexedwards/scs/v2"
)

const (
	flashError   = "flasherror"
	flashSuccess = "flashsuccess"
)

type ScsAdapter struct {
	sessionManager *scs.SessionManager
}

func New(sessionManager *scs.SessionManager) *ScsAdapter {
	return &ScsAdapter{
		sessionManager: sessionManager,
	}
}

func (sa *ScsAdapter) SetErrorFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	sa.sessionManager.Put(r.Context(), flashError, msg)
	return nil
}

func (sa *ScsAdapter) SetSuccessFlash(w http.ResponseWriter, r *http.Request, msg string) error {
	sa.sessionManager.Put(r.Context(), flashSuccess, msg)
	return nil
}

func (sa *ScsAdapter) GetFlashMessages(w http.ResponseWriter, r *http.Request) (e string, s string, err error) {
	errMsg := sa.sessionManager.PopString(r.Context(), flashError)
	successMsg := sa.sessionManager.PopString(r.Context(), flashSuccess)
	return errMsg, successMsg, nil
}
