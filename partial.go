package tman

import "html/template"

type Partial struct {
	Key    string
	Tpl    string `json:"tpl"`
	Cached *template.Template
}

func NewPartial(key string, tpl string) *Partial {
	return &Partial{Key: key, Tpl: tpl}
}
