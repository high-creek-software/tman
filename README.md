# TMan

This is a template manager for go.  Because a package named templatemanager seemed too long.

## Goals
1. Create a nice wrapper around the go template library.
2. Provide caching while in production; yet allow for hot reloading while in development
    - Caching can happen in two tiers, an embedded fs.FS and parsed templates
3. Provide a cleaner more simple api than fazer
4. Provide a merge method
5. Provide consistent errors, success, config, page metadata to the parse context
6. Add a session manager to the renderer
7. Common template functions
8. Have a page type with html page metadata
9. Have a key based access to the parsed template
10. Include partial templates

## Thoughts
The template library has a method to define specific templates and handle the caching.  However this flies in the face of easily being able to dynamically parsing in dev mode.

## Notes about embed
One of the main reasons for making tman is to make use of the go 1.16 embed directive.

My previous library used a directory, and dynamically rewrote the template paths as needed.  I wanted a cleaner approach than that for this library.

One of my main goals is goal 2 from above.  To do that, I've found there are a few things to be aware of with the embed library.

To facilitate goal to, I have a production flag that can be toggled via environment variables.  Then I just check if in production and use the appropriate fs.FS implementation:

//go:embed static
var embeddedFS
// Now to referenced the embedded directory just like the os.DirFS call below
subFS, _ := fs.Sub(embeddedFS, "static/tpls")

var templateFS fs.FS
if Prod {
    templateFS = subFS
} else {
    templateFS = os.DirFS("static/tpls")
}

### Things to Know
- Your embedded directory must be at the same level or below the source file with the embed directive.  Therefore, no `..` or `.` in the directive.